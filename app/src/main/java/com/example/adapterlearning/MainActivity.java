package com.example.adapterlearning;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String item[] = {"Naltar Valley", "Attabad-Lake", "Hunza Valley", "Naltar Valley"};
    int items[] = {R.drawable.naltar,R.drawable.attabad,R.drawable.hunza,R.drawable.naltar};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list_view);

        Adapter adapter = new Adapter(getApplicationContext(), item,items);
        listView.setAdapter(adapter);


    }
}