package com.example.adapterlearning;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.zip.Inflater;

public class Adapter extends BaseAdapter {



    Context context;
    String item[];
    int items[];
    LayoutInflater inflater;

    public Adapter(Context context, String[] item,int[] items) {
        this.context = context;
        this.item = item;
        this.items = items;
        this.inflater = (LayoutInflater.from(context));
    }


    @Override
    public int getCount() {
        return item.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.text_view, null);
        TextView textView = view.findViewById(R.id.text_view);
        ImageView imageView = view.findViewById(R.id.image_view);
        textView.setText(item[position]);
        imageView.setImageResource(items[position]);
        return view;
    }
}
